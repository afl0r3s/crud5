// Users
const GET_USERS = "GET_USERS";
const DELETE_USER = "DELETE_USER";
const GET_USER_BY_ID = "GET_USER_BY_ID";
const UPDATE_USER_BY_ID = "UPDATE_USER_BY_ID";
const POST_USER = "POST_USER";
const STATUS_CHANGE = "STATUS_CHANGE";

const TYPES = {
    GET_USERS,
    DELETE_USER,
    GET_USER_BY_ID,
    UPDATE_USER_BY_ID,
    POST_USER,
    STATUS_CHANGE,
  };
  
  export default TYPES;