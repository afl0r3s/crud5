import axios from 'axios';
import types from "../1_actions/actionTypes";

export const getUsers = () => {
	return async (dispatch) => {
	  try {
		const { data } = await axios.get(`/contact`);
		return dispatch({
		  type: types.GET_USERS,
		  payload: data,
		});
	  } catch (err) {
		console.log(err);
		return dispatch({
		  type: types.GET_USERS,
		  payload: [],
		});
	  }
	};
  };

export const addUser = (user) => {
	return async (dispatch) => {
	  try {
		const result = await axios.post(`/contact`, user);
		return dispatch({
		  type: types.POST_USER,
		  result
		});
	  } catch (err) {
		console.log(err);
	  }
	};
  };

  export const deleteUser = (userID) => {
	return async (dispatch) => {
	  try {
		const result = await axios.delete(`/contact/${userID}`);
		return dispatch({
		  type: types.DELETE_USER,
		  result
		});
	  } catch (error) {
		console.log(error);
	  }
	};
  };

  export const getUserDetails = (id) => {
	return async (dispatch) => {
	  try {
		const { data } = await axios.get(`/contact/${id}`);
		return dispatch({
		  type: types.GET_USER_BY_ID,
		  payload: data,
		});
	  } catch (error) {
		console.log(error);
	  }
	};
  };

  export const updateUser = (id, data) => {
	return async (dispatch) => {
	  try {
		const result = await axios.put(`/contact/${id}`, data);
		return dispatch({
		  type: types.UPDATE_USER_BY_ID,
		  result
		});
	  } catch (error) {
		console.log(error);
	  }
	};
  };

export const statusChange = () => {
	return {
		type: types.STATUS_CHANGE,
	};
};