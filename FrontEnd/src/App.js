import { Switch, Route } from 'react-router-dom';
import Home from './4_components/Home/home';
import UserCreate from './4_components/Home/userCreate';
import UserUpdate from './4_components/Home/userUpdate';
import Error404 from './4_components/Error404/error404';

import './App.css';

function App() {
	return (
		<div className="App">
			<Switch>
				<Route exact path="/" component={Home} />
				<Route exact path="/userCreate" component={UserCreate} />
				<Route exact path="/userUpdate/:id" component={UserUpdate} />
				<Route path="*" component={Error404} />
			</Switch>
		</div>
	);
}

export default App;
