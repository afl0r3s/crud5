import types from '../1_actions/actionTypes';

const initialState = {
	usersAll: [],//array de razas que tendra toda la info y no se usara para filtrados
	usersDetail: [],
	loading: false, //variable para mostrar un mensaje o imagen de Loading
	dataState: 'all',
};

function rootReducer(state = initialState, action) {
	//console.log("action: ",action)
	switch (action.type) {
		case types.GET_USERS:
			return {
				...state,
				usersAll:  action.payload,
				loading: false,
			};
		
		case types.POST_USER:
			return {
				...state,
			};
			
		case types.DELETE_USER:
			return {
				...state,
			};

		case types.GET_USER_BY_ID:
			return {
				...state,
				usersDetail: action.payload,
			};

		case types.UPDATE_USER_BY_ID:
			return {
				...state,
			};
			
		case types.STATUS_CHANGE:
			return {
				...state,
				loading: true,
			};

		default: {
			return state;
		}
	}
}

export default rootReducer;
