import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { Button } from '@material-ui/core';
import { BiSave, BiArrowToLeft } from "react-icons/bi";
import { NavLink } from 'react-router-dom';
import swal from 'sweetalert';
import ctgStyle from '../Style.module.css';
import { getUserDetails, updateUser } from '../../1_actions';

export default function UserCreate() {
    const dispatch = useDispatch();
    const location = useLocation();
	const history = useHistory();
    const userDetail = useSelector((state) => state.usersDetail);

    const userId = location.pathname.split("/").pop();

    useEffect(() => {
		dispatch(getUserDetails(userId));
        setInput({
            name: userDetail.name,
            email: userDetail.email,
        });
	}, [dispatch,userId,userDetail.name,userDetail.email]);

	const [input, setInput] = useState({
		name: '',
        email: '',
	});

    function handleChange(e) {
		setInput({
			...input,
			[e.target.name]: e.target.value,
		});
	}

    async function handleSubmit(e) {
		e.preventDefault();
        let message = await dispatch(updateUser(userId, input));
		if(message.result.statusText === "OK"){
			swal({
				title:'Resultado',
				text: message.result.data,
				icon: 'success',
				button: "Ok"
			})
			.then(respuesta => {
				if(respuesta) history.push('/');
			})
		}else{
			swal({
				title:'Resultado',
				text: 'Ocurrio un error !!',
				icon: 'warning',
				button: "Ok"
			})
		}
		setInput({
			name: '',
            email: '',
		});
	}

    return (
        <div className={ctgStyle.userCreate}>
            <form onSubmit={(e) => {handleSubmit(e); }} id="form1">
                <div className={ctgStyle.inputs} >
                    <input 
                        type="text"
                        name="name" 
                        value={input.name} 
                        onChange={(e) => handleChange(e)}
                        placeholder="Nombre Usuario"
                        required></input>
                    <br/>
                    <input 
                        type="text"
                        name="email" 
                        value={input.email} 
                        onChange={(e) => handleChange(e)}
                        placeholder="E-mail"
                        required></input>

                </div>
                <div className={ctgStyle.contentBtn}>
                    <Button 
                        variant="contained" 
                        className={ctgStyle.btnSave}
                        type="submit"
                        disableElevation>
                            <BiSave size="1.3em" />&nbsp;Update
                    </Button>
                    &nbsp; &nbsp;
                    <NavLink to={`/`}>
                        <Button 
                            variant="contained" 
                            className={ctgStyle.btn1}
                            type="submit"
                            disableElevation>
                                <BiArrowToLeft size="1.3em" />&nbsp;Volver
                        </Button>
                    </NavLink>
                </div>
            </form>
        </div>
    )
}
