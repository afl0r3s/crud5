import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import {BsFillPlusCircleFill, BsFillTrashFill, BsPencil } from 'react-icons/bs';
import { NavLink } from 'react-router-dom';
import { getUsers, deleteUser, statusChange } from '../../1_actions';
import swal from 'sweetalert';
import ctgStyle from '../Style.module.css';


export default function Home() {
	const usersArr = useSelector((state) => state.usersAll);
	const loading = useSelector((state) => state.loading);
	const dispatch = useDispatch();
	const history = useHistory();

	useEffect(() => {
		dispatch(getUsers());
	}, [dispatch]);

	if (loading) {
		dispatch(getUsers());
	}

	function handleClickUpdate(e) {
		history.push('/userUpdate/' + e);
	}

	function handleClickDelete(e) {
		swal({
			title:'Eliminar',
			text: 'Estas seguro que deseas borrar Usuario ?',
			icon: 'warning',
			buttons: ["No", "Si"]
		})
		.then(async (respuesta) => {
			if(respuesta) {
				let message = await dispatch(deleteUser(e));
				dispatch(statusChange());
				swal({
					title:'Aviso',
					text: message.result.data,
					icon: 'success',
					button: "Ok"
				}) 
			}
		})
	}

	return (
		<>
			<div className={ctgStyle.Catcontent}>
				<div className={ctgStyle.Catcontent2}>
					<div className={ctgStyle.Catcontent3}>
						<NavLink to="/userCreate">
							<Button variant="contained" className={ctgStyle.btn1} disableElevation>
								<BsFillPlusCircleFill size="1.1em" />
								&nbsp;Create User
							</Button>
						</NavLink>
					</div>

					<TableContainer style={{ marginTop: '10px' }} component={Paper}>
						<Table style={{ backgroundColor: 'white', width: 'auto' }} aria-label="simple table">
							<TableHead className={ctgStyle.tableHead}>
								<TableRow>
									<TableCell style={{ backgroundColor: '#AED6F1', color: 'white', minWidth: '30px' }}>
										#
									</TableCell>
									<TableCell style={{ backgroundColor: '#7FB3D5', color: 'white', minWidth: '250px' }}>
										Name
									</TableCell>
									<TableCell
										style={{ backgroundColor: '#AED6F1', color: 'white', minWidth: '250px' }}
										align="left"
									>
										E-mail
									</TableCell>
									<TableCell
										style={{ backgroundColor: '#7FB3D5', color: 'white', minWidth: '100px' }}
										align="left"
									>
										Options
									</TableCell>
								</TableRow>
							</TableHead>
						</Table>
					</TableContainer>
				</div>
			</div>

			<TableContainer style={{ marginLeft: '10px', marginTop:'110px' }} component={Paper}>
				<Table style={{ backgroundColor: 'white', width: 'auto' }} aria-label="simple table">
					<TableBody>
						{usersArr.map((row) => (
							<TableRow key={row.id}>
								<TableCell component="th" scope="row" style={{ minWidth:'30px' }}>
									{row.id}
								</TableCell>
								<TableCell component="th" scope="row" style={{ minWidth:'250px' }}>
									{row.name}
								</TableCell>
								<TableCell component="th" scope="row" style={{ minWidth:'250px' }}>
									{row.email}
								</TableCell>
								<TableCell align="right" style={{ minWidth:'100px' }}>
									<Button 
										variant="contained" 
										className={ctgStyle.btnUpdate}
										onClick={(e) => handleClickUpdate(row.id)}
										disableElevation><BsPencil size="1.1em" /></Button>
									<Button 
										variant="contained" 
										className={ctgStyle.btnDelete}
										onClick={(e) => handleClickDelete(row.id)}
										disableElevation><BsFillTrashFill size="1.1em" /></Button>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
		</>
	);
}
